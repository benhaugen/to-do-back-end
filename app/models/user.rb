class User < ApplicationRecord

  has_many :tasks

  validates :name, presence: true
  validates :name, uniqueness: true
  validates :password, presence: true
  validates :password, uniqueness: true

end
