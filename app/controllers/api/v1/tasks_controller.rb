class Api::V1::TasksController < ApplicationController

  def index
    @tasks = Task.all
    render json: @tasks
  end

  def show
    @task = Task.find_by(id: params[:id])
    render json: @task
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.find_or_create_by(task_params)
    render json: @task
  end

  def destroy
    @task = Task.find_by(id: params[:id])
    @task.destroy
  end

private

  def task_params
    params.require(:task).permit(:description, :user_id)
  end
end
